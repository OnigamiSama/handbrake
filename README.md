# HANDBRAKE BATCH SCRIPT

## How to use

Either download the file or copy/paste it in your own file. Then make it executable with `chmod +x handbrake.sh`. This script will should only on Linux

## What's inside ?

**SRC** : is the directory where your RAW data is (your uncompressed files).

**DEST** : is the directory where you want your output file to be

**DEST_EXT** : is the filename extension (.mkv or .mp4 or whatever suit you)

**HANDBRAKE_CLI** : is the linux command to run HandBrake in CLI mode so no need to change

**PRESET** : is the preset you want to use, you can see a list of all the preset with `HandBrakeCLI --preset-list`

## And now ?

With just this settings you can already beggin to work. just run the script with : `./handbrake.sh`

The script will search for files in the SRC folder, convert them to MKV files using the H.265 MKV 1080p30 preset with all original audios and subtitles and output the file in the DEST folder

If you want to go further you can use any options in the handbrakecli.txt like -m or --markers if you want to have chapter markers in your files. For more options see the Handbrake documentation at https://handbrake.fr/docs/en/latest/cli/command-line-reference.html
